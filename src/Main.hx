import hl.UI16;

class Vector2i {
    public var x:Int;
    public var y:Int;
    
    public function new(x:Int = 0, y:Int = 0) {
        this.x = x;
        this.y = y;
    }
}

class EntityState {
    public var position:Vector2i;
    public var velocity:Vector2i;

    public function new(position:Vector2i) {
        this.position = position;
        this.velocity = new Vector2i();
    }

    public function update(input:UI16) {
        velocity.x = 0;
        velocity.y = 0;
        
        if(input & Main.INPUT_LEFT == Main.INPUT_LEFT) {
            velocity.x = -2;
        }
        if(input & Main.INPUT_RIGHT == Main.INPUT_RIGHT) {
            velocity.x = 2;
        }
        if(input & Main.INPUT_UP == Main.INPUT_UP) {
            velocity.y = -2;
        }
        if(input & Main.INPUT_DOWN == Main.INPUT_DOWN) {
            velocity.y = 2;
        }

        position.x += velocity.x;
        position.y += velocity.y;
    }
}

class SimulationState {
    public var entities:Array<EntityState>;
    public var inputs:Array<UI16>;

    public function new(entities:Array<EntityState>) {
        this.entities = entities;
        this.inputs = [Main.INPUT_NONE, Main.INPUT_NONE];
    }
}

class Main extends hxd.App {
    public static inline var INPUT_NONE = 0;
    public static inline var INPUT_UP = 1;
    public static inline var INPUT_DOWN = 2;
    public static inline var INPUT_LEFT = 4;
    public static inline var INPUT_RIGHT = 8;

    private var graphics:h2d.Graphics;
    private var gameState:SimulationState;

    override function init() {
        graphics = new h2d.Graphics(s2d);
        gameState = new SimulationState([
            new EntityState(new Vector2i(100, 100)),
            new EntityState(new Vector2i(200, 200))
        ]);
    }

    static function main() {
        new Main();
    }

    private function drawEntity(entity:EntityState, color:Int) {
        graphics.beginFill(color);
        graphics.drawCircle(entity.position.x, entity.position.y, 50);
    }
    
    override function update(dt:Float) {
        //Input
        gameState.inputs[0] = INPUT_NONE;

        if(hxd.Key.isDown(hxd.Key.LEFT)) {
            gameState.inputs[0] |= INPUT_LEFT;
        }
        if(hxd.Key.isDown(hxd.Key.RIGHT)) {
            gameState.inputs[0] |= INPUT_RIGHT;
        }
        if(hxd.Key.isDown(hxd.Key.UP)) {
            gameState.inputs[0] |= INPUT_UP;
        }
        if(hxd.Key.isDown(hxd.Key.DOWN)) {
            gameState.inputs[0] |= INPUT_DOWN;
        }

        gameState.entities[0].update(gameState.inputs[0]);

        // Drawing
        graphics.clear();
        drawEntity(gameState.entities[0], 0xFF0000);
        drawEntity(gameState.entities[1], 0x0000FF);
        graphics.endFill();
    }
}
